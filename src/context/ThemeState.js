import React, {useState} from 'react';
import ThemeContext from './ThemeContext';

const ThemeState = (props) => {

    const [theme, setTheme] = useState('light');

    const toggleTheme = () => {
        setTheme(theme === 'light' ? 'dark' : 'light');
    }

    const color = theme === 'light' ? '#333' : '#fff';
    const backgroundColor = theme === 'light' ? '#fff' : '#333';

    document.body.style.color = color;
    document.body.style.backgroundColor = backgroundColor;
    return(
        <ThemeContext.Provider value={{
            theme, 
            toggleTheme
            }}>
            {props.children}
        </ThemeContext.Provider>
    );
}

export default ThemeState;