import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Landing from "../pages/Landing";
import Portfolios from "../pages/Portfolios";
import ContactMe from "../pages/ContactMe";
import Navbar from "../components/Navbar";
import Notfound from "../pages/Notfound";

const Routes = () => {
    return (
        <Router>

            <Switch>
                <Route path="/" exact>
                    <Landing />
                </Route>
                <Route path="/portfolios" exact>
                    <Portfolios />
                </Route>
                <Route path="/contactMe" exact>
                    <ContactMe />
                </Route>
                <Route path="*">
                    <Notfound />
                </Route>
            </Switch>
        </Router>
    );
}

export default Routes;