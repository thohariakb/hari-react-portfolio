import React from 'react';
import '../App.css';
import Navbar from '../components/Navbar';
import Footer from '../components/Footer';

function ContactMe() {
  return (
<>
<Navbar />
<div className="item content-container">
<form>
  <h1 className="contact-title">Contact Me</h1>

  <div className="mb-3">
    <label for="InputFirstName" className="form-label">First Name</label>
    <input type="text" className="form-control" id="InputFirstName" placeholder="Example: John" value="" />
    <label for="InputLastName" className="form-label">Last Name</label>
    <input type="text" className="form-control" id="InputLastName" placeholder="Example: Cena" />
  </div>
  <div className="mb-3">
    <label for="InputEmail" className="form-label">Email address</label>
    <input type="email" className="form-control" id="InputEmail1" placeholder="Example: emailname@gmail.com" />
  </div>
  <div className="mb-3">
    <label for="InputPhone" className="form-label">Phone Number</label>
    <input type="tel" className="form-control" id="InputPhone" placeholder="Example: 081888****" />
  </div>
  <button type="submit" id="modal-btn" className="btn btn-primary send-btn btn-shadow contact-btn">Send</button>
</form>

<div id="modals" className="modals">
  <div className="modals-header">
  </div>
  <div className="modals-content">
    <span id="close-btn" className="close-btn">&times;</span>
    <h1 style={{marginBottom: '10px', textAlign: 'center'}}>Success!</h1>
    <p style={{paddingTop: '10px'}}>Your message has been sent.</p>
    <h2>Thank you</h2>
  </div>
  <div className="modals-footer">
  </div>
</div>

</div>
<Footer></Footer>
    </>
  );
}

export default ContactMe;
